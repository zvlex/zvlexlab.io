---
title: Переименновать файлы в Linux
date: 2017-02-04 15:45 UTC
tags: linux, rename, bash
layout: post
---


Если вам потребовалось переименовать множество файлов в Linux:

```bash
for f in *.txt; do
  mv -- "$f" "${f%.txt}.log"
done
```
